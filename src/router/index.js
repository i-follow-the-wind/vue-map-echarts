import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Home')
  },
  {
    path: '/main',
    name: 'main',
    component: () => import('../views/main')
  },
  {
    path: '/one',
    name: 'one',
    component: () => import('../views/one')
  },
  {
    path: '/two',
    name: 'two',
    component: () => import('../views/two')
  },
  {
    path: '/three',
    name: 'three',
    component: () => import('../views/three')
  },
  {
    path: '*',
    redirect: 'home'
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
